1.- Programación Declarativa. Orientaciones y pautas para el estudio (1998, 1999, 2001)
```plantuml
@startmindmap
*[#red] Programación Declarativa
**_ ¿Qué es?
*** Es un estilo de programación, un paradigma
**[#Orange] ¿Cómo surge?
***[#lightgreen] Surge como reacción a algunos problemas que\n lleva consigo la programacion clasica (imperativa)
****[#lightblue] Lenguajes
***** C
***** Modula
***** Pascal
****_ Caracteristicas
***** El programador se ve obligado a dar demasiados\n detalles sobre los calculos a realizar
***** Su instrumento escencial es la asignación mediante\n la cual se modifica el estado de la memoria del ordenador\n ademas se modifica
**_ Caracteriticas
***[#lightgreen] Programas mas cortos y faciles de depurar
***[#lightgreen] Utilizada en la inteligencia artificial
***[#lightgreen] No tiene asignaciones
***[#lightgreen] El compilador toma las deciciones
**_ Tambien es llamada
***[#lightgreen] Programacion perezosa
***[#lightgreen] Programacion comoda
***[#lightgreen] Programacion hermosa
**[#Orange] Ventajas y desventajas
***[#lightgreen] Sus ventajas son
****[#lightblue] Los programas son mas eficiente y cortos
****[#lightblue] Su optimizacion es mas sencilla
****[#lightblue] Cuenta con un alto nivel de abstracción
****[#lightblue] El mantenimiento es independiente al\n desarrollo ve la aplicacion
***[#lightgreen] Sus desventajas son
****[#lightblue] Dificil de comprender
****[#lightblue] No cuenta con un pensamiento habitual
**[#Orange] Lenguajes usados
***[#lightgreen] Prolog
****[#lightblue] Creado por Alain Colmerauner y Philippe Roussel
****[#lightblue] Es un lenguaje semi interpretado
***[#lightgreen] Lisp
****[#lightblue] LISt Processor 
****_ Su sintaxis
***** Es homoiconica
****[#lightblue] Tiene notacion polaca
***[#lightgreen] Haskell 
****_ Creado por
***** Haskell Curry
****[#lightblue] Su primer version salio en 1990
***[#lightgreen] Miranda
***[#lightgreen] Erlang
***[#lightgreen] SQL
**[#Orange] Tipo de programación
***[#lightgreen] Funcional
****_ Caracteristicas
***** Usa el lenguaje de las matematicas
***** Tiene funciones de orden superior y funciones sobre funciones
***** Evaluacion peresoza
****[#lightblue] Datos infinitos
***** Los elementos son calculados en el futuro
****_ Sus caracteristicas
***** El compilador resuelve todo
*****_ Ejemplificado como
****** El trabajo del arquitecto
****_ Sus ventajas 
***** Es 10 veces mas rapido
***** Tiene mayor perspectiva
***** Se programa mejor
**[#Orange] Su logica
***[#lightgreen] Predicados con relacion entre objetos
***[#lightgreen] No tiene orden
***[#lightgreen] Tiene relacion factorial entre dos numeros
***[#lightgreen] El interprete demuestra predicados en el sistema
***[#lightgreen] No tiene distincion entre entradas y salidas
***[#lightgreen] Es orientada a pautas
****[#lightblue] Axiomas 
****[#lightblue] Modelo logico
****[#lightblue] Demostracion automatica
****[#lightblue] Reglas de inferencia
**[#Orange] Sus aplicaicones y usos
***[#lightgreen] Inteligencia artificial
***[#lightgreen] Sistemas expertos
***[#lightgreen] Procesamiento de lenguaje naturas
***[#lightgreen] Compiladores
***[#lightgreen] Bases de datos deductivas
***[#lightgreen] Acceso a bases de datos desde paginas web
@endmindmap
```

Lenguaje de Programación Funcional (2015)
```plantuml
@startmindmap
*[#red] Programacion funcional
**_ Su lenguaje imperativo
***[#lightgreen] Instrucciones 
****[#lightblue] Se ejecutan unas tras otras
****[#lightblue] Son secuenciales
****[#lightblue] Excepciones 
*****[#FFBBCC] Bucles
*****[#FFBBCC] Estructuras de control condicionales
***[#lightgreen] Programacion orientada a objetos
****[#lightblue] Se modifican por series de instrucciones
**_ Tipos de evaluacion
***[#lightgreen] Evaluacion peresoza 
***[#lightgreen] Evaluacion impaciente o estricta
****[#lightblue] Tiene inconvenientes con expeciones
***[#lightgreen] Evaluacion estricta
****[#lightblue] Unica vez
***[#lightgreen] Evaluacion memorizacion
****[#lightblue] Tiene buena memoria
****[#lightblue] Su secuencia es costosa
***[#lightgreen] Evaluacion estricta 
****[#lightblue] Es de afuera hacia adentro
****[#lightblue] No necesita conocer parametros
**[#Orange] Caracteristicas 
***[#lightgreen] Tiene variables no modificables
***[#lightgreen] Cuando se fija el dato de una variable el resultado sera el mismo
***[#lightgreen] Las constantes pueden ir como funciones
***[#lightgreen] Todas son funciones con parametros de orden superior
****[#lightblue] Funciones como parametros de otras funciones
**[#Orange] Lenguajes usados
***[#lightgreen] Scala
****_ Caracteristicas
*****[#FFBBCC] Multi paradigma
*****[#FFBBCC] Expresa patrones comunes
*****[#FFBBCC] Soporta distintas funciones
******_ Tales como
******* Anonimas
******* Orden superior
******* Anidadas
******* Currificacion
***[#lightgreen] Haskell
****_ Caracteristicas
*****[#FFBBCC] Es estandarizado
*****[#FFBBCC] Una funcion es un ciudadano de primera clase
*****[#FFBBCC] Su origen se da en base a observaciones de Haskell Curry
*****[#FFBBCC] Soporte para varios tipos de datos
*****[#FFBBCC] Integra funciones recursivas
*****[#FFBBCC] Tiene listas
*****[#FFBBCC] Tuplas
*****[#FFBBCC] Guardas
*****[#FFBBCC] Calce de patrones
***[#lightgreen] Clojure 
****_ Caracteristicas
*****[#FFBBCC] Es de proposito general
*****[#FFBBCC] Maneja el dialecto de Lisp
*****[#FFBBCC] Esta enfocado en el paradigma funcional
*****[#FFBBCC] Su fin es eliminar la complejidad de la programacion \n concurrente
**[#Orange] Ventajas y desventajas
***_ Sus ventajas 
****[#lightblue] No tiene variables y los programas no dependen \n  del parametro de entrada
****[#lightblue] Transparencia referencial
****[#lightblue] El resultado de sus funciones son independientes
****[#lightblue] Tiene mejor programacion
****[#lightblue] Tiene altos niveles de abstraccion
****[#lightblue] Tiene rapidez de compilacion
****[#lightblue] Usa datos algebraicos e haskell
****[#lightblue] Tiene seguridad e inmutabilidad
****[#lightblue] Paralelismo
****[#lightblue] Codigo mas corto y con precision
****[#lightblue] Esta optimizado
***_ Sus desventajas 
****[#lightblue] Cuenta con grandes cantidades de short lived garbage
****[#lightblue] Tiene inmutabilidad
****[#lightblue] Tiene garbage collectors
****[#lightblue] Tiende a optimizarse
****[#lightblue] Tiene memor eficiencia comparado con la imperativa
****[#lightblue] Menor eficiencia en el uso de CPU
****[#lightblue] Tienen muchas estructuras variables mutables
****[#lightblue] No es apto para realizar todas las tareas 
*****_ Debido a que
****** No es adecuado para muchas recursiones de la misma pila
****[#lightblue] Puede llegar a tener errores graves
****[#lightblue] Sus datos no son modificables como las variables
**[#Orange] Su historia
***[#lightgreen] Comienza a desarrollarse en 1956
***[#lightgreen]_ por 
****[#lightblue] John McCarthy
***[#lightgreen] En 1958 se empiza a programar la inteligencia artificial
**_ sus aplicaciones y usos
***[#lightgreen] Usado en Word
***[#lightgreen] Usado en juegos de arcade
@endmindmap
```